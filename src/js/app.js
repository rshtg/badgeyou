Parse.initialize('vgR0wQ828hbUoLcGx1cIVWjUFShYdlkAaUAM6XCa', '7igS8HM2Cc2VTwG72oxV3iEFjhL4Zr6Vyjei5mcw');

if(!!Parse.User.current() == false) {
    window.location.href = 'user.html';
}

$(document).ready(function() {

    var Grid = Backbone.View.extend({

        el: $(gridData.block),

        events: new function() {
            var events = {};

            for(var p in gridData.callers) {
                var prop = 'click ' + gridData.callers[p];
                var f = 'render' + p.substr(0, 1).toUpperCase() + p.substr(1);

                events[prop] = f;
            }
            return events;
        },
        renderMain: function(e) {
            if(!!e === false)
            {
                return;
            }

            var $this = $(e.target);
            var $tab = $(this.el);
            var $block = $(gridData.tabs.main);

            this.showCurrentBlock($this);
        },
        renderQualities: function(e) {
            if(!!e === false)
            {
                return;
            }

            var $this = $(e.target);
            var $tab = $(this.el);
            var $block = $(gridData.tabs.qualities);

            if(!!byQualities) {
                var data = {
                    quality: new function() {
                        var result = [];

                        byQualities.each(function(i, v) {
                            result.push({
                                id: v+1,
                                label: i.get('name')
                            });
                        });

                        return result;
                    }
                };

                var $data = Mustache.render(tplQualities, data);
                $block.html($data);

                var $edit = $block.find('.ifa-pencil');
                var $add = $block.find('button');
                var $remove = $block.find('.ifa-remove');

                var handler = function(o) {
                    var $list = o.find('ol');
                    var $select = o.find('select');
                    var $parent = $select.parent();
                    var $add = $parent.find('i');
                    $add.on('click', function() {
                        $list.append('<li><i>'+(parseInt($select.val())+1)+'</i>'+$select.find('option').eq($select.val()).text()+'<i class="ifa ifa-plus"></i></li>');
                    })
                };
                var onDone = function(e) {
                    var $overlay = e.data.o;
                };


                $edit.on('click.byRenders.qualities.editQuality', {
                    type: 'EditQuality',
                    handler: byRenders.qualities.editQuality,
                    onDone: byRenders.qualities.doneEditQuality,
                    onCancel: byRenders.qualities.cancelEditQuality
                }, this.showForm);
                $add.on('click.byRenders.qualities.addQuality', byRenders.qualities.addQuality);
                $remove.on('click.byRenders.qualities.removeQuality', byRenders.qualities.removeQuality);
            }

            if(!!e.type) {
                this.showCurrentBlock($this);
            }
        },
        renderActions: function(e) {
            if(!!e === false)
            {
                return;
            }

            var $this = $(e.target);
            var $tab = $(this.el);
            var $block = $(gridData.tabs.actions);

            if(!!byActions) {
                var data = {
                    action: new function() {
                        var result = [];

                        byActions.each(function(i, v) {
                            result.push({
                                id: v+1,
                                label: i.get('name')
                            });
                        });

                        return result;
                    }
                };

                var $data = Mustache.render(tplActions, data);

                $block.html($data);

                var $edit = $block.find('.ifa-pencil');
                var $add = $block.find('button');
                var $remove = $block.find('.ifa-remove');

                $edit.on('click.byRenders.actions.editAction', {
                    type: 'EditAction',
                    handler: byRenders.actions.editAction,
                    onDone: byRenders.actions.doneEditAction,
                    onCancel: byRenders.actions.cancelEditAction
                }, this.showForm);
                $add.on('click.byRenders.actions.addAction', byRenders.actions.addAction);
                $remove.on('click.byRenders.actions.removeAction', byRenders.actions.removeAction);
            }

            if(!!e.type) {
                this.showCurrentBlock($this);
            }
        },
        renderBadges: function(e) {
            if(!!e === false)
            {
                return;
            }

            var $this = $(e.target);
            var $tab = $(this.el);
            var $block = $(gridData.tabs.badges);

            if(!!byBadges) {
                var data = {
                    badge: new function() {
                        var result = [];

                        byBadges.each(function(i, v) {
                            result.push({
                                id: v+1,
                                label: i.get('name')
                            });
                        });

                        return result;
                    }
                };

                var $data = Mustache.render(tplBadges, data);

                $block.html($data);
            }

            if(!!e.type) {
                this.showCurrentBlock($this);
            }
        },
        renderTasks: function(e) {
            if(!!e === false)
            {
                return;
            }

            var $this = $(e.target);
            var $tab = $(this.el);
            var $block = $(gridData.tabs.tasks);

            this.showCurrentBlock($this);
        },
        renderQuests: function(e) {
            if(!!e === false)
            {
                return;
            }

            var $this = $(e.target);
            var $tab = $(this.el);
            var $block = $(gridData.tabs.quests);

            this.showCurrentBlock($this);
        },
        renderRoles: function(e) {
            if(!!e === false)
            {
                return;
            }

            var $this = $(e.target);
            var $tab = $(this.el);
            var $block = $(gridData.tabs.roles);

            if(!!byRoles) {
                var data = {
                    role: new function() {
                        var result = [];

                        byRoles.each(function(i, v) {
                            result.push({
                                id: v+1,
                                label: i.get('name')
                            });
                        });

                        return result;
                    }
                };

                var $data = Mustache.render(tplRoles, data);

                $block.html($data);

                var $edit = $block.find('.ifa-pencil');
                var $add = $block.find('button');
                var $remove = $block.find('.ifa-remove');

                $edit.on('click.byRenders.roles.editRole', {
                    type: 'EditRole',
                    handler: byRenders.roles.editRole,
                    onDone: byRenders.roles.doneEditRole,
                    onCancel: byRenders.roles.cancelEditRole
                }, this.showForm);
                $add.on('click.byRenders.roles.addRole', byRenders.roles.addRole);
                $remove.on('click.byRenders.roles.removeRole', byRenders.roles.removeRole);
            }

            if(!!e.type) {
                this.showCurrentBlock($this);
            }
        },
        renderNominations: function(e) {
            if(!!e === false)
            {
                return;
            }

            var $this = $(e.target);
            var $tab = $(this.el);
            var $block = $(gridData.tabs.nominations);

            this.showCurrentBlock($this);
        },
        showCurrentBlock: function($this) {
            var anchor = window.location.hash.substr(1) !== '' ? window.location.hash.substr(1) : 'main';

            console.log($this);
            $this = $($this.find('a').get(0));
            if($this) {
                anchor = $this.attr('href').substr(1);
            } else {
                $this = $('#by-call-'+anchor);
            }

            var $tab = $(gridData.tab);
            var $block = $('#'+anchor);
            var $active = $tab.find('.active');

            if($active !== $block)
            {
                var $title = $(gridData.title);
                $title.text($this.text());

                $active.removeClass('active');
                $block.toggleClass('active');
            }
        },
        showForm: function(e) {
            var data = e.data;

            var $parent = $(this).parent();
            var id = parseInt($parent.find('i').eq(0).text())-1;console.log(id);

            var tpl = '';
            var render = [];
            switch(data.type)
            {
                case 'EditQuality':
                    tpl = tplEditQuality;
                    render = byQualities.getTplData(id);
                    break;
                case 'EditAction':
                    tpl = tplEditAction;
                    render = byActions.getTplData(id);
                    break;
                case 'EditRole':
                    tpl = tplEditRole;
                    render = byRoles.getTplData(id);
                    break;
                default: break;
            }

            var $overlay = $(Mustache.render(tplOverlay, {content: ''}));

            $('body').eq(0).append($overlay);

            $overlay.css({
                lineHeight: $(window).height()+'px'
            });

            var $content = $overlay.find('div').eq(1);

            $content.html(Mustache.render(tpl, render));

            var $submit = $content.find('.by-overlay-submit');
            var $cancel = $content.find('.by-overlay-cancel');

            if(!!data.handler) {
                data.handler($content);
            }

            $overlay.on('click.grid.showForm', function(e) {
                if(!!e.target) {
                    if($(e.target).attr('id') === $overlay.attr('id')) {
                        $overlay.remove();
                    }
                }
            });

            if(!!data.onDone) {
                $submit.on('click.onDone', data.onDone);
            }
            if(!!data.onCancel) {
                $cancel.on('click.onCancel', data.onCancel);
            }
        }
    });


    var grid = new Grid();
    BadgeYou.loadData(grid, grid.showCurrentBlock);
});