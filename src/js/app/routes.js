var Router = {
    routes: {
        '':'main',
        '#qualities':'qualities',
        '#actions':'actions',
        '#badges':'tasks',
        '#tasks':'tasks',
        '#quests':'quests',
        '#roles':'roles',
        '#nominations':'nominations',
        '#login':'log_in',
        '#signup':'sign_up'
    }
};

Router = Backbone.Router.extend(Router);

var byRouter = new Router();

Backbone.history.start();
