var Quality = {
    name: '',
    roles: []
};
var Action = {
    name: '',
    roles: []
};
var Badge = {
    name: '',
    image: '',
    preview: ''
};
var Task = {};//@todo:Task.model
var Quest = {};//@todo:Quest.model
var Role = {
    name: '',
    qualities: [],
    actions: []
};
var Nomination = {
    name: '',
    type: '',
    timeStarts: 0,
    timeEnds: 0,
    status: 1
};

/* grid */
var gridData = {
    header: '#by-header',
    logo: '#by-logo',
    admin: '#by-admin',
    navigation: '#by-navigation',
    menu: '#by-menu',
    menuList: '#by-menu-list',
    callers: {
        main: '#by-call-main',
        qualities: '#by-call-qualities',
        actions: '#by-call-actions',
        badges: '#by-call-badges',
        tasks: '#by-call-tasks',
        quests: '#by-call-quests',
        roles: '#by-call-roles',
        nominations: '#by-call-nominations'
    },
    block: '#by-main',
    title: '#by-section-title',
    tab: '#by-tab',
    tabs: {
        main: '#main',
        qualities: '#qualities',
        actions: '#actions',
        badges: '#badges',
        tasks: '#tasks',
        quests: '#quests',
        roles: '#roles',
        nominations: '#nominations'
    },
    overlay: '#by-overlay-screen'
};
