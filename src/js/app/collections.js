var Qualities = {};//an object of Quality.model
var Actions = {};//an object of Action.model
var Badges = {};//an oject of Badge.model
var Tasks = {};//@todo:Task.collection from Task.model
var Quests = {};//@todo.Quests.collection from Quest.model
var Roles = {};//an object of Role.model
var Nominations = {};//an object of Nomination.model

/* QUALITY */
var Quality = Backbone.Model.extend({
    defaults: Quality
});
var Qualities = Backbone.Collection.extend({
    model: Quality,
    searchQuality: function(name) {
        return this.find(function(m) {
            var n = m.get('name');
            return n == name ? n : false;
        });
    },
    addQuality: function(name, data) {
        var result = this.searchQuality(name);
        if(!result) {
            var object = {
                name: name
            };

            for(var d in data) {
                object[d] = data[d];
            }
            this.push(object);
        }

        return result;
    },
    deleteQuality: function(id) {
        return this.remove(this.models[id]);
    },
    getTplData: function(id) {
        var result = [];
        var model = this.models[id];

        var _uses_roles = model.get('roles');
        var uses_roles = [];
        for(var i = 0; i < _uses_roles.length; i++)
        {
            if(!!byRoles.models[_uses_roles]) {
                var label = byRoles.models[_uses_roles].get('name');
                if(!!label) {
                    uses_roles.push({
                        id: _uses_roles,
                        label: label
                    });
                }
            }
        }

        var roles = new function() {
            var r = [];
            byRoles.each(function(i, v) {
                r.push({
                    id: v,
                    label: i.get('name')
                });
            });
            return r;
        };

        result = {
            quality: model.get('name'),
            uses_roles: uses_roles,
            roles: roles
        };

        return result;
    }
});
/* ACTION */
var Action = Backbone.Model.extend({
    defaults: Action
});
var Actions = Backbone.Collection.extend({
    model: Action,
    searchAction: function(name) {
        return this.find(function(m) {
            var n = m.get('name');
            return n == name ? n : false;
        });
    },
    addAction: function(name, data) {
        var result = this.searchAction(name);
        if(!result) {
            var object = {
                name: name
            };

            for(var d in data) {
                object[d] = data[d];
            }
            this.push(object);
        }

        return result;
    },
    deleteAction: function(id) {
        return this.remove(this.models[id]);
    },
    getTplData: function(id) {
        var result = [];
        var model = this.models[id];

        var _uses_roles = model.get('roles');
        var uses_roles = [];
        for(var i = 0; i < _uses_roles.length; i++)
        {
            if(!!byRoles.models[_uses_roles]) {
                var label = byRoles.models[_uses_roles].get('name');
                if(!!label) {
                    uses_roles.push({
                        id: _uses_roles,
                        label: label
                    });
                }
            }
        }

        var roles = new function() {
            var r = [];
            byRoles.each(function(i, v) {
                r.push({
                    id: v,
                    label: i.get('name')
                });
            });
            return r;
        };

        result = {
            uses_roles: uses_roles,
            roles: roles
        };

        return result;
    }
});
/* BADGES */
var Badge = Backbone.Model.extend({
    defaults: Badge
});
var Badges = Backbone.Collection.extend({
    model: Badge,
    searchBadge: function(name) {
        return this.find(function(m) {
            var n = m.get('name');
            return n == name ? n : false;
        });
    }
});
/* TASKS */
var Task = Backbone.Model.extend({
    defaults: Task
});
var Tasks = Backbone.Collection.extend({
    model: Task,
    searchTask: function(name) {
        return this.find(function(m) {
            var n = m.get('name');
            return n == name ? n : false;
        });
    }
});
/* QUESTS */
var Quest = Backbone.Model.extend({
    defaults: Quest
});
var Quests = Backbone.Collection.extend({
    model: Quest,
    searchQuest: function(name) {
        return this.find(function(m) {
            var n = m.get('name');
            return n == name ? n : false;
        });
    }
});
/* ROLES */
var Role = Backbone.Model.extend({
    defaults: Role
});
var Roles = Backbone.Collection.extend({
    model: Role,
    searchRole: function(name) {
        return this.find(function(m) {
            var n = m.get('name');
            return n == name ? n : false;
        });
    },
    addRole: function(name, data) {
        var result = this.searchRole(name);
        if(!result) {
            var object = {
                name: name
            };

            for(var d in data) {
                object[d] = data[d];
            }
            this.push(object);
        }

        return result;
    },
    deleteRole: function(id) {
        return this.remove(this.models[id]);
    },
    getTplData: function(id) {
        var result = [];
        var model = this.models[id];
        console.log(model, id);

        var _uses_qualities = model.get('qualities');
        var uses_qualities = [];
        for(var i = 0; i < _uses_qualities.length; i++)
        {
            if(!!byQualities.models[_uses_qualities])
            {
                var label = byQualities.models[_uses_qualities].get('name');
                if(!!label) {
                    uses_qualities.push({
                        id: _uses_qualities,
                        label: label
                    });
                }
            }
        }

        var qualities = new function() {
            var q = [];
            byQualities.each(function(i, v) {
                q.push({
                    id: v,
                    label: i.get('name')
                });
            });
            return q;
        };

        var _uses_actions = model.get('actions');
        var uses_actions = [];
        for(var i = 0; i < _uses_actions.length; i++)
        {
            if(!!byActions.models[_uses_actions]) {
                var label = byActions.models[_uses_actions].get('name');
                if(!!label) {
                    uses_actions.push({
                        id: _uses_actions,
                        label: label
                    });
                }
            }
        }

        var actions = new function() {
            var a = [];
            byActions.each(function(i, v) {
                a.push({
                    id: v,
                    label: i.get('name')
                });
            });
            return a;
        };

        result = {
            uses_qualities: uses_qualities,
            qualities: qualities,
            uses_actions: uses_actions,
            actions: actions
        };
        console.log(result);

        return result;
    }
});
/* NOMINATIONS */
var Nomination = Backbone.Model.extend({
    defaults: Nomination
});
var Nominations = Backbone.Collection.extend({
    model: Nomination,
    searchNomination: function(name) {
        return this.find(function(m) {
            var n = m.get('name');
            return n == name ? n : false;
        });
    }
});
