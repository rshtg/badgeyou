var byQualities = [];
var byActions = [];
var byBadges = [];
var byTasks = [];
var byQuests = [];
var byRoles = [];
var byNominations = [];

var BadgeYou = {
    sources: [
        'src/data/qualities.json',
        'src/data/actions.json',
        'src/data/badges.json',
        'src/data/tasks.json',
        'src/data/quests.json',
        'src/data/roles.json',
        'src/data/nominations.json'
    ],
    dataLoadStatus: 0,//wait for 7 files
    loadData: function(grid) {
        //below is async loading...
        $(gridData.overlay).css({
            lineHeight: $(window).height()+'px'
        });

        $.getJSON(this.sources[0])
            .done(function(data) {
                byQualities = new Qualities(data);
                byQualities.bind('add remove', grid.renderQualities.bind(grid));

                this.completeLoadData();
            }.bind(this))
            .fail(function(e){
                this.completeLoadData();
                console.log(e.status);
            }.bind(this));
        $.getJSON(this.sources[1])
            .done(function(data) {
                byActions = new Actions(data);
                byActions.bind('add remove', grid.renderActions.bind(grid));

                this.completeLoadData();
            }.bind(this))
            .fail(function(e){
                this.completeLoadData();
                console.log(e.status);
            }.bind(this));
        $.getJSON(this.sources[2])
            .done(function(data) {
                byBadges = new Badges(data);
                byBadges.bind('add remove', grid.renderBadges.bind(grid));

                this.completeLoadData();
            }.bind(this))
            .fail(function(e){
                this.completeLoadData();
                console.log(e.status);
            }.bind(this));
        $.getJSON(this.sources[3])
            .done(function(data) {
                byTasks = new Tasks(data);
                byTasks.bind('add remove', grid.renderTasks.bind(grid));

                this.completeLoadData();
            }.bind(this))
            .fail(function(e){
                this.completeLoadData();
                console.log(e.status);
            }.bind(this));
        $.getJSON(this.sources[4])
            .done(function(data) {
                byQuests = new Quests(data);
                byQuests.bind('add remove', grid.renderQuests.bind(grid));

                this.completeLoadData();
            }.bind(this))
            .fail(function(e){
                this.completeLoadData();
                console.log(e.status);
            }.bind(this));
        $.getJSON(this.sources[5])
            .done(function(data) {
                byRoles = new Roles(data);
                byRoles.bind('add remove', grid.renderRoles.bind(grid));

                this.completeLoadData();
            }.bind(this))
            .fail(function(e){
                this.completeLoadData();
                console.log(e.status);
            }.bind(this));
        $.getJSON(this.sources[6])
            .done(function(data) {
                byNominations = new Nominations(data);
                byNominations.bind('add remove', grid.renderNominations.bind(grid));

                this.completeLoadData();
            }.bind(this))
            .fail(function(e){
                this.completeLoadData();
                console.log(e.status);
            }.bind(this));
    },
    completeLoadData: function() {
        var status = this.dataLoadStatus;
        status++;
        this.dataLoadStatus = status;

        if(status === this.sources.length) {
            $(gridData.overlay).fadeOut('slow', function() {
                this.remove();
            })
        }
    }
};

var byRenders = {
    qualities: {
        addQuality: function() {
            var $parent = $(this).parent();
            var $input = $parent.find('input');

            var name = $input.val();
            if(!!name) {
                byQualities.addQuality(name);
            }
        },
        removeQuality: function() {
            var $this = $(this);
            var $parent = $this.parent();

            var id = parseInt($parent.find('i').eq(0).text());
            id = id-1;

            byQualities.deleteQuality(id);
        },
        editQuality: function() {

        },
        doneEditQuality: function() {

        },
        cancelEditQuality: function() {

        }
    },
    actions: {
        addAction: function() {
            var $parent = $(this).parent();
            var $input = $parent.find('input');

            var name = $input.val();
            if(!!name) {
                byActions.addAction(name);
            }
        },
        removeAction: function() {
            var $this = $(this);
            var $parent = $this.parent();

            var id = parseInt($parent.find('i').eq(0).text());
            id = id-1;

            byActions.deleteAction(id)
        },
        editAction: function() {

        },
        doneEditAction: function() {

        },
        cancelEditAction: function() {

        }
    },
    roles: {
        addRole: function() {
            var $parent = $(this).parent();
            var $input = $parent.find('input');

            var name = $input.val();
            if(!!name) {
                byRoles.addRole(name);
            }
        },
        removeRole: function() {
            var $this = $(this);
            var $parent = $this.parent();

            var id = parseInt($parent.find('i').eq(0).text());
            id = id-1;

            byRoles.deleteRole(id);
        },
        editRole: function() {

        },
        doneEditRole: function() {

        },
        cancelEditRole: function() {

        }
    }
};